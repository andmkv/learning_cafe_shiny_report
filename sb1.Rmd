---
title: "R Notebook"
output: html_notebook
---

```{r}
library(readxl)
library(tidyverse)

# Загрузим справочник кафе

cafes <- read_excel("cafes.xlsx", sheet = "Справочник") %>% mutate(Type = as.factor(Type)) %>% separate(Coords, c("coordY", "coordX"), sep = ", ")


receipts <- read_excel("receipts_jan_2020.xlsx", sheet = "receipts")

plan2020 <- read_excel("plan2020.xlsx", sheet = "plan2020")

plan2020 <- plan2020 %>% mutate(Date = as.Date(Date)) %>% complete(Date = seq.Date(as.Date('2020-01-01'), as.Date('2020-01-31'), by = "day"), Cafe) %>% group_by(Cafe) %>% fill(Plan)

plan2020 <- plan2020 %>% left_join(cafes, by = c("Cafe" = "Cafe_nm"))

receipts_agg <- receipts %>% group_by(Cafe_id, Date) %>% summarise(total = sum(Cost * Amount), count = n(), avg = mean(Cost * Amount))

report_df <- receipts_agg %>% mutate(Date = as.Date(Date)) %>% left_join(plan2020, by = c("Cafe_id" = "Cafe_id", "Date" = "Date"))

pivot_df <- as.data.frame(report_df) %>% select(Cafe, Type, Date, total, count, avg, Plan) %>% mutate(plan_perc = total / Plan * 100.0)
colnames(pivot_df) <- c("Кафе", "Тип", "Дата", "Выручка", "Кол-во чеков", "Средний чек", "План по выр.", "% выполн.")

```

